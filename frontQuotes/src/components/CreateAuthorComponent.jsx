import React, { useState } from "react";
import axios from "../service/Service";
import { useHistory } from "react-router-dom";

const CreateAuthorComponent = () => {
  const history = useHistory();
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    var author = {
      firstName: firstName,
      lastName: lastName,
    };
    axios.post("/authors", author);
    history.push("/");
  };

  return (
    <div className="col-md-6 offset-md-3 offset-md-3">
      <form>
        <h2>Add author</h2>
        <div className="form-group">
          <label className="label">First name:</label>
          <input
            type="text"
            className="form-control"
            name="firstName"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label className="label">Last name:</label>
          <input
            type="text"
            className="form-control"
            name="lastName"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
          />
        </div>
        <div>
          <button className="btn btn-primary" onClick={handleSubmit}>
            Save
          </button>
          <button
            className="btn btn-danger"
            style={{ marginLeft: "20px" }}
            onClick={() => {
              history.push("/");
            }}
          >
            Cancel
          </button>
        </div>
      </form>
    </div>
  );
};

export default CreateAuthorComponent;
