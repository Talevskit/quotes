import React, { useState, useEffect } from "react";
import axios from "../service/Service";
import { useHistory } from "react-router-dom";

const AuthorListComponent = () => {
  const history = useHistory();
  const [authors, setAuthors] = useState([]);

  useEffect(() => {
    axios.get("/authors").then((res) => {
      setAuthors(res.data);
    });
  }, []);

  const deleteHandler = (id) => {
    axios.delete(`/authors/${id}`).then((res) => {
      setAuthors(authors.filter((author) => id !== author.id));
    });
  };

  const deleteQuote = (id) => {
    axios.delete(`/quotes/${id}`);
  };

  return (
    <div className="container">
      {authors.map((auth) => (
        <div
          key={auth.author.authorId}
          className="row p-2 border border-secondary m-2"
          style={{ backgroundColor: "#cdcdb1", borderRadius: "10px" }}
        >
          <h2 className="col-5">
            {auth.author.firstName + " " + auth.author.lastName}
          </h2>
          <button
            className="btn btn-primary col-2"
            style={{ marginLeft: "20px" }}
            onClick={() => history.push(`/author/${auth.author.authorId}`)}
          >
            Edit
          </button>
          <button
            className="btn btn-secondary col-2"
            style={{ marginLeft: "20px" }}
            onClick={() => history.push(`/quote/${auth.author.authorId}`)}
          >
            Add Quote
          </button>
          <button
            className="btn btn-danger col-2"
            style={{ marginLeft: "20px" }}
            onClick={() => deleteHandler(auth.author.authorId)}
          >
            Delete
          </button>
          <ul className="col-12">
            {auth.quotes.map((q) => (
              <li key={q.quoteId} className=" m-1">
                <div
                  className="row m-2"
                  style={{ backgroundColor: "#b9b992", borderRadius: "10px" }}
                >
                  <h6 className="col-10 mt-2">{q.quote}</h6>
                  <button
                    className="btn btn-danger m-1"
                    onClick={() => deleteQuote(q.quoteId)}
                  >
                    Delete
                  </button>
                </div>
              </li>
            ))}
          </ul>
        </div>
      ))}
    </div>
  );
};
export default AuthorListComponent;
