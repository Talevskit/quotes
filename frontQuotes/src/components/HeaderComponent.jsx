import React from "react";
import { useHistory } from "react-router-dom";

const HeaderConponent = () => {
  const history = useHistory();

  return (
    <div>
      <header>
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
          <div>
            <p className="navbar-brand">Stoicism</p>
            <button
              className="btn btn-secondary"
              onClick={() => history.push("/author")}
            >
              Add new author
            </button>
          </div>
        </nav>
      </header>
    </div>
  );
};

export default HeaderConponent;
