import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";

import axios from "../service/Service";

const EditAuthorComponent = () => {
  const history = useHistory();
  const { id } = useParams();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");

  const handleUpdate = () => {
    let updated = {
      firstName: firstName,
      lastName: lastName,
    };
    axios.put(`/authors/${id}`, updated);
    history.push("/");
  };

  return (
    <div className="container col-6">
      <form onSubmit={handleUpdate}>
        <h3>First name</h3>
        <input
          className="form-control mt-3"
          type="text"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
        ></input>
        <h3>Last name</h3>
        <input
          className="form-control mt-3"
          type="text"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
        ></input>
        <input type="submit" className="btn btn-primary col-12 mt-3" />
      </form>
    </div>
  );
};

export default EditAuthorComponent;
