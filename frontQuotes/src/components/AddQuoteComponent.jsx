import axios from "../service/Service";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";

const AddQuoteComponent = () => {
  const [quote, setQuote] = useState("");
  const { id } = useParams();
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    let quoteObj = {
      quote: quote,
      id: id,
    };
    axios.post("/quotes", quoteObj);
    history.push("/");
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <h2>Add quote</h2>
        <div className="form-group">
          <label className="label">Quote:</label>
          <input
            type="text"
            className="form-control"
            value={quote}
            onChange={(e) => setQuote(e.target.value)}
          />
        </div>
        <div>
          <input type="submit" className="btn btn-primary" />
          <button
            className="btn btn-danger"
            style={{ marginLeft: "20px" }}
            onClick={() => {
              history.push("/");
            }}
          >
            Cancel
          </button>
        </div>
      </form>
    </div>
  );
};

export default AddQuoteComponent;
