import axios from "axios";

const API = "http://localhost:8080/api/v1";

export default axios.create({ baseURL: API });
