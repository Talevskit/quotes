import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HeaderComponent from "./components/HeaderComponent";
import FooterComponent from "./components/FooterComponent";
import AuthorListComponent from "./components/AuthorListComponent";
import CreateAuthorComponent from "./components/CreateAuthorComponent";
import AddQuoteComponent from "./components/AddQuoteComponent";
import EditAuthorComponent from "./components/EditAuthorComponent";

const App = () => {
  return (
    <div className="App">
      <Router>
        <HeaderComponent />
        <div className="container">
          <Switch>
            <Route exact path="/" component={AuthorListComponent}></Route>
            <Route path="/author/:id" component={EditAuthorComponent}></Route>
            <Route path="/quote/:id" component={AddQuoteComponent}></Route>
            <Route path="/author" component={CreateAuthorComponent}></Route>
          </Switch>
        </div>
        <FooterComponent />
      </Router>
    </div>
  );
};

export default App;
