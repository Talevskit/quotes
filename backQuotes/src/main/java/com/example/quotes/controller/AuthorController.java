package com.example.quotes.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.quotes.entity.Author;
import com.example.quotes.entity.AuthorDTO;
import com.example.quotes.service.AuthorService;

@RestController
@RequestMapping("/api/v1/authors")
@CrossOrigin(origins = "http://localhost:3000")
public class AuthorController {

	@Autowired
	private AuthorService authorService;
	

	@PostMapping
	public Author addAuthor(@RequestBody Author author) {
		return authorService.addAuthor(author);
	}

	@GetMapping("/{id}")
	public AuthorDTO getAuthor(@PathVariable("id") Integer id) {
		return authorService.getAuthor(id);
	}

	@GetMapping
	public Iterable<AuthorDTO> getAuthors() {
		return authorService.getAuthors();
	}

	@DeleteMapping("/{id}")
	public boolean deleteAuthor(@PathVariable("id") Integer id) {
		authorService.deleteAuthor(id);
		return true;
	}
	
	@PutMapping("/{id}")
	public Author updateAuthor(@RequestBody Author author, @PathVariable("id") Integer id) {
		return authorService.update(author, id);
	}
}
