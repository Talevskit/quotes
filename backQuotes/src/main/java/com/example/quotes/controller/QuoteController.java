package com.example.quotes.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.quotes.entity.Quote;
import com.example.quotes.entity.QuoteDTO;
import com.example.quotes.service.QuoteService;

@RestController
@RequestMapping("/api/v1/quotes")
@CrossOrigin(origins = "http://localhost:3000")
public class QuoteController {

	@Autowired
	private QuoteService quoteService;

	@PostMapping
	public Quote addQuote(@RequestBody QuoteDTO quoteDTO) {
		return quoteService.addQuote(quoteDTO);
	}

	@GetMapping("/{id}")
	public Optional<Quote> getQuote(@PathVariable("id") Integer id) {
		return quoteService.getQuote(id);
	}

	@GetMapping
	public Iterable<Quote> getQuotes() {
		return quoteService.getQuotes();
	}

	@DeleteMapping("/{id}")
	public boolean deleteQuote(@PathVariable("id") Integer id) {
		quoteService.deleteQuote(id);
		return true;
	}

	@PutMapping("/{id}")
	public Quote updateQuote(@RequestBody Quote quote, @PathVariable("id") Integer id) {
		return quoteService.updateQuote(quote, id);

	}

}
