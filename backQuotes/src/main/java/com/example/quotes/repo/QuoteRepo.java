package com.example.quotes.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.quotes.entity.Quote;

@Repository
public interface QuoteRepo extends JpaRepository<Quote,Integer>{

}
