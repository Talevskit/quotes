package com.example.quotes.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.quotes.entity.Author;

@Repository
public interface AuthorRepo extends JpaRepository<Author,Integer>{

}
