package com.example.quotes.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.quotes.entity.Author;
import com.example.quotes.entity.AuthorDTO;
import com.example.quotes.entity.Quote;
import com.example.quotes.repo.AuthorRepo;
import com.example.quotes.repo.QuoteRepo;

@Service
public class AuthorService {

	@Autowired
	private AuthorRepo authorRepo;
	@Autowired
	private QuoteRepo quoteRepo;

	public Author addAuthor(Author author) {
		return authorRepo.save(author);
	}

	public Iterable<AuthorDTO> getAuthors() {
		List<AuthorDTO> authors = new ArrayList<AuthorDTO>();
		authorRepo.findAll().stream().forEach(author -> {
			AuthorDTO dto = new AuthorDTO();
			dto.setAuthor(author);
			dto.setQuotes(author.getQuotes());
			authors.add(dto);
		});
		return authors;
	}

	public AuthorDTO getAuthor(Integer id) {
		AuthorDTO dto = new AuthorDTO();
		List<Quote> quotes = new ArrayList<Quote>();
		Author author = authorRepo.findById(id).get();
		dto.setAuthor(author);
		quoteRepo.findAll().stream().forEach(quote -> {
			if(quote.getAuthor().getAuthorId() == id)
				quotes.add(quote);
		});
		dto.setQuotes(quotes);
		return dto;
		
	}
	
	public boolean deleteAuthor(Integer id) {
		authorRepo.deleteById(id);
		return true;
	}

	public Author update(Author author, Integer id) {
		Author updated = authorRepo.findById(id).get();
		updated.setFirstName(author.getFirstName());
		updated.setLastName(author.getLastName());
		return authorRepo.save(updated);
	}

}
