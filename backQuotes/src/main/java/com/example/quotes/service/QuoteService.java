package com.example.quotes.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.quotes.entity.Quote;
import com.example.quotes.entity.QuoteDTO;
import com.example.quotes.repo.AuthorRepo;
import com.example.quotes.repo.QuoteRepo;

@Service
public class QuoteService {
	@Autowired
	private QuoteRepo quoteRepo;
	@Autowired
	private AuthorRepo authorRepo;

	public Quote addQuote(QuoteDTO quoteDTO) {
		Quote quote = new Quote();
		quote.setQuote(quoteDTO.getQuote());
		quote.setAuthor(authorRepo.findById(quoteDTO.getId()).get());
		return quoteRepo.save(quote);

	}

	public List<Quote> getQuotes() {
		return quoteRepo.findAll();
	}

	public Optional<Quote> getQuote(int id) {
		return quoteRepo.findById(id);
	}

	public boolean deleteQuote(int id) {
		quoteRepo.deleteById(id);
		return true;
	}

	public Quote updateQuote(Quote quote, Integer id) {
		Quote updated = quoteRepo.findById(id).get();
		updated.setQuote(quote.getQuote());
		if (updated.getAuthor() == null)
			updated.setAuthor(updated.getAuthor());
		return quoteRepo.save(updated);
	}
}
